# Pontarlier à vélo 🚲

<img src="https://codeberg.org/lejun/pontarlier-a-velo/raw/branch/master/img/opengraph.jpg" width="300"/>

Pontarlier à vélo est une carte de Pontarlier, montrant comme seules informations les temps de parcours à vélo entre divers points d’intérêt de la région pontissalienne.
Les points sont choisis pour leur pertinence, cela peut être des places importantes, des monuments connus, des carrefours, des mairies…

Les temps de trajets sont renseignés à titre indicatif, mais sont destinés à être affinés avec le temps, pour correspondre le plus possible à l’expérience de cyclistes normaux. Pas des bêtes de course, ou des vélos à assistance électrique. Je tiens en créant ce projet à être le plus utile possible aux néo-cyclistes qui viennent juste de sortir leur vélo de la cave, de l’asso', ou du bouclard.

## Comment aider ?

Pour améliorer cette carte, toutes les suggestions sont acceptées. La meilleure manière de proposer un nouveau point d’intérêt, un nouveau trajet, ou une nouvelle estimation de temps, est d'ouvrir un ticket (onglet *Issues* puis bouton vert — il faut avoir un compte).

**Pour proposer un point d’intérêt**

Avec Firefox (testé sur version 76) : ouvrir les Outils de développement (appuyer sur F12), puis sélectionner la Console javascript. En cliquant sur la carte, un texte apparaît dans la console, `clic au point {x:xxx, y: yyy}`, où `x` et `y` sont les coordonnées du point sur la carte.

Avec un logiciel prenant en charge les fichiers SVG : ouvrir [le fond de carte](https://codeberg.org/lejun/pontarlier-a-velo/src/branch/master/_graphics/map_export.svg), et trouver la fonction donnant les coordonnées du curseur ou d'un point donné.

Reportez les coordonnées dans un message où il faudra aussi renseigner le nom du lieu proposé, et si possible expliquer sa pertinence. Évidemment on va éviter les lieux « Chez moi lol », mais on va aussi limiter le nombre de lieux pour ne pas rendre la carte trop dense.

**Pour proposer un nouveau trajet ou une estimation de temps**

Indiquez bien le trajet en question, et n’hésitez pas à préciser votre niveau à vélo. Novice, cycliste confirmé·e, pro… l’avis de tout le monde sera pris en compte, en gardant bien à l'esprit que l'outil est à destination des cyclistes qui n’ont pas encore l'habitude du vélo.

**Pour faire remonter un bug**

Hé oui, derrière les sites web il y a des personnes humaines, il est possible que des choses ne marchent pas bien (par exemple sur mobile, c'est dur à bien faire sur mobile), donc là aussi on peut laisser un message (même canaux de communication).

## Transposer cette carte pour une autre ville

Pour adapter cette page à une autre ville, c’est assez simple à condition de connaître le HTML et des bases de JS. Tout le code présent est réutilisable, à condition de respecter la [license GNU General Public License v3.0](https://codeberg.org/lejun/pontarlier-a-velo/src/branch/master/LICENSE), c’est à dire qu’il est obligatoire de repartager tout code fait à partir de celui-ci sous la même licence.

### Fond de carte

Le fond de carte est contenu dans l’élément SVG `g#bg_map`, qui est le premier élément du `svg#map` dans le fichier `index.html`. Ce contenu SVG a été généré par un programme d’édition vectoriel ([Inkscape](https://inkscape.org/fr/) en l'occurrence, mais Adobe Illustrator, Sketch ou Affinity Designer), puis passé dans l’outil [SVGOMG](https://jakearchibald.github.io/svgomg/) pour enlever les données superflues. Attention, lors de l’édition de `index.html`, il ne faut pas enlever les éléments à la fin de `svg#map`, ils sont utiles pour l’exécution du script. Une fois les éléments SVG rajoutés, pensez aussi à modifier l’attribut `viewBox` de l'élément `svg#map`, pour correspondre avec les valeurs du SVG de votre carte.

### Points et lignes

Une fois que le fond de carte est en place et qu’il s’affiche bien avec le zoom et tout ça, il est temps de placer les points et les trajets.

Toutes ces données sont contenues dans le fichier `js/data.js`. Ce fichier contient un objet `data`, qui contient 3 éléments :

- `defaultPointsDisplayed` est un tableau qui contient le nom des points dont le nom est affiché par défaut ;
- `lines` est un tableau qui contient des objets définissant chaque ligne ;
- `points` est un objet qui contient des objets définissant les coordonnées de chaque point.

Chaque point est défini par des coordonnées `x` et `y` relatives aux coordonnées du fichier SVG. Il ne s’agit pas de coordonnées latitude/longitude. Pour les trouver, il y a un événement de clic sur la surface du SVG, qui retourne les coordonnées dudit clic en console (ouvrir la console via les outils de développement F12). *Attention : cette manipulation ne marche qu’avec [Firefox](https://www.mozilla.org/fr/firefox/new/) (testé sur la version 76), à cause de la différence d'implémentation de `UIEvent.layerX` et `UIEvent.layerY`*.

Avec ces coordonnées on peut ajouter les objets de points sur le modèle

```
    "Nom du Point": {
      x: 144,
      y: 288,
      label: "Nom du point", // facultatif
    },
```

En adaptant bien sûr le nom du point et les coordonnées.

L’argument `label` est facultatif, au cas où on veut un nom de variable différent du nom du point.

Une fois qu’assez de points ont été ajoutés, on peut tracer les lignes.

Chaque objet de ligne est composé comme suit :

- pour un trajet plat

```
    {
      start: "Nom du Point A",
      end: "Nom du Point B",
      difficulty: 0,
      displayMin: true,        // facultatif
      time: 6,
      align: "top",            // facultatif
      className: "",           // facultatif
    }
```

- pour un trajet en montée/descente

```
    {
      start: "Nom du Point C",
      end: "Nom du Point D",
      difficulty: 1,
      displayMin: true,            // facultatif
      time: 6,
      times: { hard: 7, easy: 5 },
      align: "top",                // facultatif
      className: "",               // facultatif
    },
```

Chacune de ces propriétés doivent être définies :

- `start` et `end` sont les noms exacts des points tels que définis dans l’objet `points` ;
- `difficulty` renseigne la difficulté : `0` pour un chemin plat, `1` pour une montée entre le point C et D, `-1` pour une descente (il y a quelques imprécisions dans le code qui font que les valeurs sont parfois prises à l’envers, c'est un bug à résoudre—si ça marche pas du premier coup ça marchera en inversant la valeur) ;
- `displayMin` spécifie si “min” est ajouté après le chiffre de la durée *par défaut sur `true`* ;
- `time` définit le temps de trajet affiché pour terrain plat et utilisé pour tous les calculs d’itinéraires ;
- `times` est un objet qui n’est *nécessaire que pour les trajets en montée/descente*, il contient deux valeurs :
    * `hard` la durée du trajet en montée ;
    * `easy` la durée du trajet en descente ;
- `align` précise si le texte de la durée est affiché au dessus ou au dessous de la ligne (ne marche que pour les trajets plats) *par défaut sur "top"* ;
- `className` est facultatif, et va transmettre la même `class` aux éléments de la ligne, et de la zone de clic correspondant à la ligne.

L’affichage des points et des lignes ne se fera pas complètement si les données ne sont pas bien renseignées.

## Arborescence

```
├── css
├── fonts
│   └── …
├── _graphics
├── img
└── js
```

### /css

Feuilles de style pour la mise en page du site.

### /fonts

Contient la fonte WorkSans utilisée pour la mise en page.

### /_graphics

Contient le fond de carte de la zone d'intérêt.

### /img

Contient uniquement le fichier `opengraph.jpg` qui est une pré-visualisation de l'outil.

### js

Contient le cœur du projet. `data.js` est le fichier où sont renseignés tous les nœuds et temps de trajets. Le reste étant des morceaux de code.

## Remerciements

C’est l'infographie « [Les temps de parcours à vélo dans Paris](http://www.leparisien.fr/info-paris-ile-de-france-oise/transports/greve-dans-les-transports-a-paris-les-temps-de-parcours-a-velo-en-une-infographie-21-12-2019-8222538.php) » du Parisien qui a lancé la cyclosphère début 2020.

La fonte [Work Sans](https://github.com/weiweihuanghuang/Work-Sans) a été tracée par Wei Huang, et est distribuée sous license libre.

Une partie des données cartographiques utilisées ainsi que certains contenus vectoriels dans les fichiers d’images sources sont issus d’[OpenStreetMap](https://www.openstreetmap.org), et sont partagés sous leur licence originelle spécifique. Ces contenus sont sous © les cartographes d’OpenStreetMap.

[@briacp](https://github.com/briacp) et [@joachimesque](https://github.com/joachimesque) sont à l'origine du [code](https://github.com/joachimesque/paris-a-velo) dont est issue cette branche.

TafLeVélo a publié [son propre code](https://gitlab.com/taflevelo/temps-velo) quasiment en même temps que celui de Joachim, sous licence MIT plutôt que GPL. Son projet génère une carte moins humaine de par l'utilisation de nombreux automatismes.
